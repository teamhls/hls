﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncNet;

namespace EncodingSlave
{
    public class RemoteServerPeer : IPeer
    {
        public CUserToken token { get; private set; }

        public RemoteServerPeer(CUserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
        }


        /// <summary>
        /// 메시지를 수신했을 때 호출된다.
        /// 파라미터로 넘어온 버퍼는 워커 스레드에서 재사용 되므로 복사한 뒤 어플리케이션으로 넘겨준다.
        /// </summary>
        /// <param name="buffer"></param>
        void IPeer.on_message(Const<byte[]> buffer)
        {
            // 버퍼를 복사한 뒤 CPacket클래스로 감싼 뒤 넘겨준다.
            // CPacket클래스 내부에서는 참조로만 들고 있는다.
            byte[] app_buffer = new byte[buffer.Value.Length];
            Array.Copy(buffer.Value, app_buffer, buffer.Value.Length);
            CPacket msg = new CPacket(app_buffer, this);
            ScheduleService.client.enqueue_packet(msg);
        }

        void IPeer.on_removed()
        {
            ScheduleService.client.enqueue_packet_EVENT(NETWORK_EVENT.disconnected);
        }

        void IPeer.send(CPacket msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
        }

        void IPeer.process_user_operation(CPacket msg)
        {
            PROTOCOL protocol_id = (PROTOCOL)msg.pop_protocol_id();
            switch (protocol_id)
            {
                case PROTOCOL.TASK_ENCODING_START:
                {
                    Console.WriteLine("encoding start\n");
                    string user_id = msg.pop_string();
                    int seq_num = msg.pop_int32();
                    string file_name = msg.pop_string();

                    Encoder encoder = new Encoder();
                    encoder.encoding(file_name);

                    CPacket res_msg = CPacket.create((short)PROTOCOL.TASK_ENCODING_SUCCESS);
                    res_msg.push(user_id);
                    res_msg.push(seq_num);
                    ScheduleService.client.send(res_msg);
                    break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncNet;

namespace ScheduleServer
{
    public class User : IPeer
    {
        CUserToken token;
        /* user */
        public string userid;

        public User(CUserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
            this.userid = null;
        }

        void IPeer.on_message(Const<byte[]> buffer)
        {
            byte[] clone = new byte[1024];
            Array.Copy(buffer.Value, clone, buffer.Value.Length);
            CPacket msg = new CPacket(clone, this);
            Program.server.enqueue_packet(msg);
        }

        void IPeer.on_removed()
        {
            Console.WriteLine("The client disconnected.");
            Program.remove_user(this);
        }

        public void send(CPacket msg)
        {
         //   Console.WriteLine("protocol id " + (PROTOCOL)msg.protocol_id);
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.socket.Disconnect(false);
        }

        void IPeer.process_user_operation(CPacket msg)
        {
            PROTOCOL protocol_id = (PROTOCOL)msg.pop_protocol_id();
            switch (protocol_id)
            {
                case PROTOCOL.USER_BEGIN:
                {
                    
                    userid = msg.pop_string();
                    Console.WriteLine(userid);
                    break;

                }
                /* user */
                case PROTOCOL.META_SEND :
                {
                    string filename = msg.pop_string();
                    string duration = msg.pop_string();
                    string resolution = msg.pop_string();
                    Program.server.sManager.process_meta(this, filename, duration, resolution);

                       
                    CPacket res_meg = CPacket.create((short)PROTOCOL.META_SEND_RESPONSE);
                    res_meg.push(filename);
                    res_meg.push("10");
                    this.send(res_meg);

                    Console.WriteLine(filename);

                    break;    
                }

                case PROTOCOL.UPLOAD_START:
                {
                    
                    for (int i = 0; i < 5; i++)
                    {
                        Job j =  Program.server.sManager.find_job(this.userid, i);
                        if (j == null) break;

                        j.state = JobState.UPLOADING;
                        CPacket res_msg = CPacket.create((short)PROTOCOL.TASK_UPLOAD_START);
                        res_msg.push(j.seq_num);
                        res_msg.push(j.file_name);
                        this.send(res_msg);
                    }
                    break;
                }

                case PROTOCOL.TASK_UPLOAD_SUCCESS:
                {
                    int i = msg.pop_int32();
                    Job j = Program.server.sManager.find_job(this.userid, i);
                    j.state = JobState.ENCODING;
                    CPacket res_msg = CPacket.create((short)PROTOCOL.TASK_ENCODING_START);
                    res_msg.push(j.user_id);
                    res_msg.push(j.seq_num);
                    res_msg.push(j.file_name);
                    j.owner.send(res_msg);

                    int seq_num = msg.pop_int32();
                    j = Program.server.sManager.find_job(this.userid, seq_num);
                    if (j != null)
                    {
                        res_msg = CPacket.create((short)PROTOCOL.TASK_UPLOAD_START);
                        res_msg.push(j.seq_num);
                        res_msg.push(j.file_name);
                        this.send(res_msg);
                    }

                    break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncNet;

namespace ScheduleServer
{
    class Program
    {
        public static List<User> userlist;
        public static List<Slave> slavelist;
        public static int slave_cnt;

        public static Server server = new Server();

        static void Main(string[] args)
        {
            CPacketBufferManager.initialize(2000);
            slave_cnt = 1;
            userlist = new List<User>();
            slavelist = new List<Slave>();
            CNetworkService service = new CNetworkService();
            service.session_created_callback += on_session_created;
            service.initialize();
            service.listen("0.0.0.0", 9000, 100);

            Console.WriteLine("server On!");
            while (true)
            {
                string input = Console.ReadLine();
                System.Threading.Thread.Sleep(1000);
            }


        }

        static void on_session_created(CUserToken token)
        {
            if (slavelist.Count < slave_cnt)
            {
                Slave slave = new Slave(token);
                lock (userlist)
                {
                    Console.WriteLine("slave connect!");
                    slavelist.Add(slave);
                }
            }
            else
            {
                User user = new User(token);
                lock (userlist)
                {
                    Console.WriteLine("user connect!");
                    userlist.Add(user);
                }
            }
            
        }

        /* 클라이언트에서 종료요청 */
        public static void remove_user(User user)
        {
            lock (userlist)
            {
                userlist.Remove(user);
            }
        }

        public static void remove_slave(Slave user)
        {
            lock (slavelist)
            {
                slavelist.Remove(user);
            }
        }
    }
}

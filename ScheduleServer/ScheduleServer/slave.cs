﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncNet;

namespace ScheduleServer
{
    public class Slave: IPeer
    {
        CUserToken token;

        /* slave */
        public List<Job> joblist;
        public string domain;

        public Slave(CUserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
            this.joblist = new List<Job>();
            this.domain = null;
        }

        public Job find_job(string user_id, int seq_num)
        {
            foreach (Job j in joblist)
            {
                if (j.user_id == user_id && j.seq_num == seq_num)
                {
                   // joblist.Remove(j);
                    return j;
                }
            }

            return null;
        }

        void IPeer.on_message(Const<byte[]> buffer)
        {
            byte[] clone = new byte[1024];
            Array.Copy(buffer.Value, clone, buffer.Value.Length);
            CPacket msg = new CPacket(clone, this);
            Program.server.enqueue_packet(msg);
        }

        void IPeer.on_removed()
        {
            Console.WriteLine("The client disconnected.");
            Program.remove_slave(this);
        }

        public void send(CPacket msg)
        {
         //   Console.WriteLine("protocol id " + (PROTOCOL)msg.protocol_id);
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.socket.Disconnect(false);
        }

        void IPeer.process_user_operation(CPacket msg)
        {
            PROTOCOL protocol_id = (PROTOCOL)msg.pop_protocol_id();
            switch (protocol_id)
            {

                 /* slave */
                case PROTOCOL.SLAVE_BEGIN :
                {
                    domain = msg.pop_string();
                    break;
                }
                case PROTOCOL.TASK_ENCODING_SUCCESS:
                {
                    string user_id = msg.pop_string();
                    int seq_num = msg.pop_int32();

                    Job j = find_job(user_id, seq_num);
                    if (j!=null) joblist.Remove(j);

                    Console.WriteLine(j.file_name + " encoding success");
                    break;
                }
            }
        }
    }
    
}

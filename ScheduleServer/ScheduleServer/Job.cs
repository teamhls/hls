﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleServer
{

    public enum JobState{
        IDLE = 0,
        UPLOADING = 1,
        ENCODING = 2,
    }
    public class Job
    {
        public string user_id;
        public string file_name;
        public JobState state;
        public Slave owner;

        public int seq_num;
   
        public Job(Slave owner, string id, string file_name, int num)
        {
            user_id = id;
            this.file_name = file_name;
            seq_num = num;
            state = JobState.IDLE;
            this.owner = owner;
        }

        public int cost()
        {
            return 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ScheduleServer
{
    public class ScheduleManager
    {

        public ScheduleManager()
        {
        }

        public void process_meta(User user, string filename, string duration, string resolution)
        {
            makeFile_m3u8(user, filename, duration);
        }

        public void makeFile_m3u8(User user, string filename, string duration){
            List<Slave> slavelist = Program.slavelist;
            int serverNum = slavelist.Count;
            
            int full_duration, segment_duration = 10;
            string FName = filename.Split('.')[0];
            string[] _temp = FName.Split('\\');
            FName = _temp[_temp.Length - 1];
            FName = user.userid + "_" + FName;

            //string m3u8;

            List<string> m3u8 = new List<string>();
            m3u8.Add("#EXTM3U");
            m3u8.Add("#EXT-X-TARGETDURATION:" + segment_duration.ToString());

            string[] temp =  duration.Split(':');
            
            full_duration = 3600 * int.Parse(temp[0]);
            full_duration+= 60 * int.Parse(temp[1]);
            full_duration+= int.Parse(temp[2]);


            
            int i=0;
            while(full_duration > 0){
                Slave slave = slavelist[i%serverNum];

                slave.joblist.Add(new Job(slave, user.userid, FName + i.ToString() + "." + filename.Split('.')[1], i));
                    
                m3u8.Add("#EXTINF:"+segment_duration+",");
                m3u8.Add(slave.domain + FName + i.ToString() + ".ts");
                
                full_duration -= segment_duration;
                i++;
            }

            m3u8.Add("#EXT-X-ENDLIST");


            System.IO.File.WriteAllLines(@"C:\Users\Yumas\433\etc\HLS\" + FName + ".m3u8", m3u8.ToArray());
            Console.WriteLine(m3u8);   
        }
        public Job find_job(string user_id, int seq_num)
        {
            List<Slave> slavelist = Program.slavelist;
            foreach (Slave s in slavelist)
            {
                Job j = s.find_job(user_id, seq_num);
                if (j != null) return j;
            }
            return null;
        }
        
        public void add_slave(User slave)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncNet;
using System.Threading;

namespace ScheduleServer
{
    class Server
    {
        object operation_lock;
        Queue<CPacket> user_operations;
        Thread logic_thread;
        AutoResetEvent loop_event;
        public ScheduleManager sManager;

        public Server()
        {
            this.sManager = new ScheduleManager();
            this.operation_lock = new object();
            this.loop_event = new AutoResetEvent(false);
            this.user_operations = new Queue<CPacket>();
            this.logic_thread = new Thread(loop);
            this.logic_thread.Start();
        }

        void loop()
        {
            while (true)
            {
                CPacket packet = null;
                lock (this.operation_lock)
                {
                    if (this.user_operations.Count > 0)
                    {
                        packet = this.user_operations.Dequeue();
                    }
                }

                if (packet != null)
                {
                    // 패킷 처리.
                    process_receive(packet);
                }

                // 더이상 처리할 패킷이 없으면 스레드 대기.
                if (this.user_operations.Count <= 0)
                {
                    this.loop_event.WaitOne();
                }
            }
        }
        public void enqueue_packet(CPacket packet)
        {
            lock (this.operation_lock)
            {
                this.user_operations.Enqueue(packet);
                this.loop_event.Set();
            }

        }

        void process_receive(CPacket msg)
        {
            //todo:
            // user msg filter 체크.
            msg.owner.process_user_operation(msg);
        }
        public void user_disconnected(User user)
        {
            ((IPeer)user).disconnect();

        }
    }
}

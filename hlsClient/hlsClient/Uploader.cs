﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace hlsClient
{
    public class Uploader
    {
        public void upload(string fullpath, int seq_num, string upload_filename)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri("ftp://10.100.50.10/upload/" + upload_filename));
            request.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;


            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential ("Anonymous","");
            
            // Copy the contents of the file to the request stream.
            string[] temp = fullpath.Split('.');
            string FName = temp[0] + seq_num.ToString() + "." + temp[1];

            FileStream stream = File.OpenRead(FName);
            byte[] fileContents = new byte[stream.Length];
            stream.Read(fileContents, 0, fileContents.Length);
            stream.Close();

            request.ContentLength = fileContents.Length;
            Stream requestStream = request.GetRequestStream();

            Console.WriteLine("uploading...");
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
    
            Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);
    
            response.Close();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using AsyncNet;

namespace hlsClient
{
    public class splitter
    {
        public string filename;
        public string duration;

        public void split(string srcFile, string duration)
        {
            Console.WriteLine(srcFile);

            string extension = srcFile.Split('.')[1];
            Process p = runCommand(srcFile.Split('.')[0], duration, extension);
            Console.WriteLine("Split complete\n");
        }

        public Process runCommand(string filename, string seg_duration, string extension)
        {
            Console.WriteLine("process start!");
            ProcessStartInfo psiProcInfo = new ProcessStartInfo();

            Process prc = new Process();

            psiProcInfo.FileName = @"C:\Users\Yumas\433\etc\HLS\ffmpeg\ffmpeg\bin\ffmpeg.exe";
            psiProcInfo.Arguments = " -i " + filename+"."+extension + " -acodec copy -f segment -segment_time " + seg_duration 
                + " -vcodec copy -reset_timestamps 1 -map 0 -an " + filename+"%d."+extension;

            Console.WriteLine(psiProcInfo.FileName + psiProcInfo.Arguments);
            psiProcInfo.UseShellExecute = false;
            psiProcInfo.WindowStyle = ProcessWindowStyle.Hidden;
            psiProcInfo.RedirectStandardError = true;
            psiProcInfo.RedirectStandardOutput = true;
            psiProcInfo.CreateNoWindow = true;

            prc.StartInfo = psiProcInfo;
            prc.Start();

            prc.WaitForExit();

            return prc;
        }
    }
}


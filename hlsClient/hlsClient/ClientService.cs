﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using AsyncNet;

namespace hlsClient
{
    public class ClientService
    {
        public static TcpClient client;
        public ClientService()
        {
            
        }

        public void start(){
            client = new TcpClient(this);
            
            client.connect("127.0.0.1", 9000);
            

        //    client.disconnect();
        }

        public void connected_callback()
        {
            user_id_send("hihi");
            meta_send();
        }

        public void user_id_send(string user_id)
        {
            Console.WriteLine(user_id);
            CPacket msg = CPacket.create((short)PROTOCOL.USER_BEGIN);
            msg.push(user_id);
            client.send(msg);
        }
        public void meta_send()
        {
            // get meta data with ffmpeg command
            metainfo meta = new metainfo();
            RemoteServerPeer.r.current_file = meta;

            meta.getMeta(@"C:\Users\Yumas\433\etc\HLS\sample.mp4");
            
            Console.WriteLine(meta.fullpathname);
            Console.WriteLine(meta.duration);
            Console.WriteLine(meta.resolution);

            CPacket msg = CPacket.create((short)PROTOCOL.META_SEND);
            msg.push(meta.fullpathname);
            msg.push(meta.duration);
            msg.push(meta.resolution);
            client.send(msg);
        }
    }
}

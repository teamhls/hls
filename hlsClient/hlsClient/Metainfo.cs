﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace hlsClient
{
    public class metainfo
    {
        public string filename;
        public string duration;
        public string resolution;
        public string fullpathname;

        public metainfo()
        {
            this.filename = null;
            this.duration = null;
            this.resolution = null;
        }

        public void getMeta(string srcFile)
        {
            fullpathname = srcFile;
            string resolution = "";
            string duration = "";
            Process p = runCommand(srcFile);

            do
            {
                
                string line = p.StandardOutput.ReadLine();
                if (line.Contains("Media Duration"))
                {
                    int index = line.IndexOf(":");
                    int start = index + 2;

                    duration = line.Substring(start, line.Length - start);
                }

                if (line.Contains("Image Size"))
                {
                    int index = line.IndexOf(":");
                    int start = index + 2;

                    resolution = line.Substring(start, line.Length - start);
                }
            } while (!p.StandardOutput.EndOfStream);

            string[] temp = srcFile.Split('\\');
            string filename = temp[temp.Length - 1];

            this.filename = filename;
            this.resolution = resolution;
            this.duration = duration;
        }


        Process runCommand(string command)
        {
            Console.WriteLine("process start!");
            ProcessStartInfo psiProcInfo = new ProcessStartInfo();

            Process prc = new Process();

            psiProcInfo.FileName = @"C:\Users\Yumas\433\etc\exiftool-10.00\exiftool.exe";
            psiProcInfo.Arguments = command;
            psiProcInfo.UseShellExecute = false;
            psiProcInfo.WindowStyle = ProcessWindowStyle.Hidden;
            psiProcInfo.RedirectStandardError = true;
            psiProcInfo.RedirectStandardOutput = true;
            psiProcInfo.CreateNoWindow = true;

            prc.StartInfo = psiProcInfo;
            prc.Start();

            return prc;
        }
    }
}

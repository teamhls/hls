﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using AsyncNet;
using System.Net;
using System.Net.Sockets;

namespace hlsClient
{
    public enum NETWORK_EVENT : byte
    {
        // 접속 완료.
        connected,

        // 연결 끊김.
        disconnected,

        none,

        // 끝.
        end
    }

    public class TcpClient
    {
        IPeer server; // 연결된 서버 객체.
        CNetworkService service; // TCP통신을 위한 서비스 객체
        ClientService cService;

        Thread logic_thread;
        AutoResetEvent loop_event;
        object operation_lock;
        Queue<CPacket> user_operations;
        Queue<NETWORK_EVENT> network_events_operations;

        public TcpClient(ClientService cservice)
        {
            CPacketBufferManager.initialize(10);

            this.operation_lock = new object();
            this.loop_event = new AutoResetEvent(false);
            this.user_operations = new Queue<CPacket>();
            this.network_events_operations = new Queue<NETWORK_EVENT>();

            this.logic_thread = new Thread(loop);
            this.logic_thread.Start();

            this.cService = cservice;
        }

        public void connect(string host, int port)
        {
            if (this.service != null)
            {
                Console.WriteLine("Already connected.");
                return;
            }

            // CNetworkService객체는 메시지의 비동기 송,수신 처리를 수행한다.
            this.service = new CNetworkService();

            // endpoint정보를 갖고있는 Connector생성. 만들어둔 NetworkService객체를 넣어준다.
            CConnector connector = new CConnector(service);
            // 접속 성공시 호출될 콜백 매소드 지정.
            connector.connected_callback += on_connected_server;
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(host), port);
            connector.connect(endpoint);
        }

        void on_connected_server(CUserToken server_token)
        {
            this.server = new RemoteServerPeer(server_token);

            // 유니티 어플리케이션으로 이벤트를 넘겨주기 위해서 매니저에 큐잉 시켜 준다.
            this.enqueue_packet_EVENT(NETWORK_EVENT.connected);
        }

        public void send(CPacket msg)
        {
            try
            {
                this.server.send(msg);
                CPacket.destroy(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void disconnect()
        {
            if (this.server != null)
            {
                
                ((RemoteServerPeer)this.server).token.disconnect();
            }
        }



        public void enqueue_packet(CPacket packet)
        {
            lock (this.operation_lock)
            {
                this.user_operations.Enqueue(packet);
                this.loop_event.Set();
            }

        }

        public void enqueue_packet_EVENT(NETWORK_EVENT packet)
        {
            lock (this.operation_lock)
            {
                this.network_events_operations.Enqueue(packet);
                this.loop_event.Set();
            }

        }


        void loop()
        {
            while (true)
            {
                CPacket packet = null;
                NETWORK_EVENT _event = NETWORK_EVENT.none;
                lock (this.operation_lock)
                {
                    if (this.user_operations.Count > 0)
                    {
                        packet = this.user_operations.Dequeue();
                    }
                    else if (this.network_events_operations.Count > 0)
                    {
                        _event = this.network_events_operations.Dequeue();
                    }
                }

                if (packet != null)
                {
                    // 패킷 처리.
                    process_receive(packet);
                }
                else if (_event != NETWORK_EVENT.none)
                {
                    process_receive_event(_event);
                }

                // 더이상 처리할 패킷이 없으면 스레드 대기.
                if (this.user_operations.Count <= 0 && this.network_events_operations.Count <= 0)
                {
                    this.loop_event.WaitOne();
                }
            }
        }

        void process_receive(CPacket msg)
        {
            this.server.process_user_operation(msg);
        }

        void process_receive_event(NETWORK_EVENT status)
        {
            switch (status)
            {
                // 접속 성공.
                case NETWORK_EVENT.connected:
                {
                    Console.WriteLine("connect!");
                    cService.connected_callback();
                    break;
                }
                // 연결 끊김.
                case NETWORK_EVENT.disconnected:
                {
                    Console.WriteLine("disconnect!");
                    break;
                }

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsyncNet;

namespace hlsClient
{
    public class RemoteServerPeer : IPeer
    {
        public static RemoteServerPeer r = null;
        public CUserToken token { get; private set; }

        public metainfo current_file;

        public RemoteServerPeer(CUserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
            this.current_file = null;
            r = this;
        }


        /// <summary>
        /// 메시지를 수신했을 때 호출된다.
        /// 파라미터로 넘어온 버퍼는 워커 스레드에서 재사용 되므로 복사한 뒤 어플리케이션으로 넘겨준다.
        /// </summary>
        /// <param name="buffer"></param>
        void IPeer.on_message(Const<byte[]> buffer)
        {
            // 버퍼를 복사한 뒤 CPacket클래스로 감싼 뒤 넘겨준다.
            // CPacket클래스 내부에서는 참조로만 들고 있는다.
            byte[] app_buffer = new byte[buffer.Value.Length];
            Array.Copy(buffer.Value, app_buffer, buffer.Value.Length);
            CPacket msg = new CPacket(app_buffer, this);
            ClientService.client.enqueue_packet(msg);
        }

        void IPeer.on_removed()
        {
            ClientService.client.enqueue_packet_EVENT(NETWORK_EVENT.disconnected);
        }

        void IPeer.send(CPacket msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
        }

        void IPeer.process_user_operation(CPacket msg)
        {
            PROTOCOL protocol_id = (PROTOCOL)msg.pop_protocol_id();
            switch (protocol_id)
            {
                case PROTOCOL.META_SEND_RESPONSE:
                {
                    string filename = msg.pop_string();
                    string seg_duration = msg.pop_string();
                    splitter split = new splitter();
                    split.split(filename, seg_duration);


                    CPacket res_msg =CPacket.create((short)PROTOCOL.UPLOAD_START);
                    ClientService.client.send(res_msg);
                    break;
                }
                case PROTOCOL.TASK_UPLOAD_START:
                {
                    int seq_num = msg.pop_int32();
                    string upfileName = msg.pop_string();
                    Uploader uploader = new Uploader();
                    uploader.upload(current_file.fullpathname, seq_num, upfileName);

                    CPacket res_msg = CPacket.create((short)PROTOCOL.TASK_UPLOAD_SUCCESS);
                    res_msg.push(seq_num);
                    res_msg.push(seq_num + 5);
                    ClientService.client.send(res_msg);

                    break;
                }
            }
        }
    }
}

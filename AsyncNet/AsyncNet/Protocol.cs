﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsyncNet
{
    public enum PROTOCOL : short
    {
        META_SEND = 0,
        SLAVE_BEGIN = 1,
        USER_BEGIN = 2,
        
        TASK_SEND = 3,
        META_SEND_RESPONSE = 4,

        UPLOAD_START = 5,

        TASK_UPLOAD_START = 6,
        TASK_UPLOAD_SUCCESS = 7,
        TASK_ENCODING_START = 8,
        TASK_ENCODING_SUCCESS = 9,

        END
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace AsyncNet
{
    public struct Const<T>
    {
        public T Value { get; private set; }

        public Const(T value)
            : this()
        {
            this.Value = value;
        }
    }
    /* 가장 말단에서 서버와 클라이언트의 연결(세션)을 관리 */
    public class CUserToken
    {
        public Socket socket { get; set; }

        public SocketAsyncEventArgs receive_event_args { get; private set; }
        public SocketAsyncEventArgs send_event_args { get; private set; }

        // 바이트를 패킷 형식으로 해석해주는 해석기.
        CMessageResolver message_resolver;

        // session객체. 어플리케이션 딴에서 구현하여 사용.
        IPeer peer;
        // 전송할 패킷을 보관해놓는 큐. 1-Send로 처리하기 위한 큐이다.
        Queue<CPacket> sending_queue;
        private object cs_sending_queue;

        public CUserToken()
        {
            this.cs_sending_queue = new object();

            this.message_resolver = new CMessageResolver();
            this.peer = null;
            this.sending_queue = new Queue<CPacket>();
        }
        public void set_peer(IPeer peer)
        {
            this.peer = peer;
        }
        public void set_event_args(SocketAsyncEventArgs receive_event_args, SocketAsyncEventArgs send_event_args)
        {
            this.receive_event_args = receive_event_args;
            this.send_event_args = send_event_args;
        }

        //받는 메시지 재조립과정
        public void on_receive(byte[] buffer, int offset, int transfered)
        {
            this.message_resolver.on_receive(buffer, offset, transfered, on_message);
        }
        void on_message(Const<byte[]> buffer)
        {
            if (this.peer != null)
            {
                this.peer.on_message(buffer);
            }
        }
        public void on_removed()
        {
            lock (this.cs_sending_queue)
            {
                this.sending_queue.Clear();
            }

            if (this.peer != null)
            {
                this.peer.on_removed();
            }
        }

        public void send(CPacket msg)
        {
            lock (this.cs_sending_queue)
            {
                CPacket clone = new CPacket();
                msg.copy_to(clone);
                if (this.sending_queue.Count <= 0)
                {
                    this.sending_queue.Enqueue(clone);
                    start_send();
                    return;
                }
                Console.WriteLine("Queue is not empty. Copy and Enqueue a msg. protocol id : " + msg.protocol_id);
                this.sending_queue.Enqueue(clone);
            }

        }

        /* 패킷처리! */
        void start_send()
        {
            lock (this.cs_sending_queue)
            {
                // 전송이 아직 완료된 상태가 아니므로 데이터만 가져오고 큐에서 제거하진 않는다.
                CPacket msg = this.sending_queue.Peek();
                msg.record_size();
                this.send_event_args.SetBuffer(this.send_event_args.Offset, msg.position); //이걸 새로하면 문제가??? 버퍼 최대사이즈를 고려해야됨!
                Array.Copy(msg.buffer, 0, this.send_event_args.Buffer, this.send_event_args.Offset, msg.position);
                bool pending = this.socket.SendAsync(this.send_event_args);
                if (!pending)
                {
                    process_send(this.send_event_args);
                }
            }
        }


        public void process_send(SocketAsyncEventArgs e)
        {
            lock (this.cs_sending_queue)
            {
                if (e.BytesTransferred <= 0 || e.SocketError != SocketError.Success)
                {
                    Console.WriteLine(string.Format("Failed to send. error {0}, transferred {1}", e.SocketError, e.BytesTransferred));
                    if (this.sending_queue.Count > 0 && this.socket.Connected)
                    {
                        start_send();
                    }
                    else
                    {
                        this.sending_queue.Dequeue();
                    }
                    return; // 재전송?
                }

            
                // count가 0이하일 경우는 없겠지만...
                if (this.sending_queue.Count <= 0)
                {
                    Console.WriteLine("Sending queue count is less than zero!");
                    return;
                    
                }

                /* 다 못보냈을경우 더 보내기?? */
                int size = this.sending_queue.Peek().position;
                if (e.BytesTransferred != size)
                {
                    string error = string.Format("Need to send more! transferred {0},  packet size {1}", e.BytesTransferred, size);
                    Console.WriteLine(error);
                    return;
                }
                this.sending_queue.Dequeue();
                if (this.sending_queue.Count > 0)
                {
                    start_send();
                }
            }
        }
        public void disconnect()
        {
            try
            {
                this.socket.Shutdown(SocketShutdown.Send);
            }
            catch (Exception) { }
            this.socket.Close();
        }
    }
}
